### Introduction
	
This app:

i)Creates a foreground service for monitoring location updates and adds them to an sqlite db.

ii)Upon inserting each location item, it determines its 2 closest neighbors by a list of predefined location points. This list contains locations as well as hardcoded weights, indicating the order of these points.

iii)Now, when the user enters a statically defined geofence, the app determines whether the user moves anodically or cathodically by comparing each location-update-item's 2 closest neighbors distance with the next location-update-item's 2 closest neighbors.

Anode means going from the bigger weight to the smaller one
Cathode means going from smaller weight to the bigger one

### Creating a Geofence

Latitude, Longitude, radius

expiration duration (in milliseconds) or fallback to a default expiration duration

### Monitoring Location

Interval (how often to check for location) - Helps battery

Fastest interval (how often to receive a location update coming from another app)

Max wait time (wait for this long, until you get a batch of location updates) - Helps battery

Max location age (when the device has already fetched a location before we ask for updates, determine if it is old)

Smallest displacement (minimum distance between 2 location updates) - Helps battery


### List of constants

public interface REQUEST_CODE {
int UPDATE_GOOGLE_SERVICES = 775;
int ENABLE_LOCATION_POPUP = 776;
int LOCATION_PERMISSION = 777;
}

public interface LOCATION_REQUEST {
int INTERVAL = 60000; // ms
int FASTEST_INTERVAL = 10000; // ms
int MAX_WAIT_TIME = INTERVAL * 3; // ms
long MAX_AGE = 5 * 60 * 1000; // 5 minutes
float SMALLEST_DISPLACEMENT = 1000; // meters

}

public interface WORK_NAMAGER {
// Work manager
String NAME = "gr.atcom.directionrnd.common.location.LocationWorker";

}

public interface NOTIFICATION {
int ID = 4;
String CHANNEL_ID = "gr.atcom.directionrnd.common.location.NotificationChannelId";
}

public interface GEOFENCE {
/**
 * For this sample, geofences expire after twelve hours.
 */
long EXPIRATION_IN_HOURS = 12;
long EXPIRATION_IN_MILLISECONDS = EXPIRATION_IN_HOURS * 60 * 60 * 1000;
float RADIUS_IN_METERS = 1609; // 1 mile, 1.6 km
}
