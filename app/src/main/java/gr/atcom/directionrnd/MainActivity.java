package gr.atcom.directionrnd;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Map;

import gr.atcom.directionrnd.common.geofence.AccidentModel;
import gr.atcom.directionrnd.common.geofence.GeofenceBroadcastReceiver;
import gr.atcom.directionrnd.common.geofence.GeofenceErrorMessages;
import gr.atcom.directionrnd.common.location.EnableLocationPopup;
import gr.atcom.directionrnd.common.location.FusedLocationUtil;
import gr.atcom.directionrnd.common.location.GoogleApiClientUtil;
import gr.atcom.directionrnd.common.permissions.PermissionAction;
import gr.atcom.directionrnd.common.permissions.PermissionDelegate;
import gr.atcom.directionrnd.common.util.Definitions;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClientUtil.IGoogleApiClientUtil, EnableLocationPopup.IEnableLocationPopup,
        PermissionAction, OnCompleteListener<Void> {

    @Nullable private GeofencingClient mGeofencingClient;
    @Nullable private PendingIntent mGeofencePendingIntent;
    @NonNull private ArrayList<Geofence> mGeofenceList;

    @NonNull private final GoogleApiClientUtil googleApiClientUtil;
    @NonNull private final EnableLocationPopup enableLocationPopup;
    @NonNull private final FusedLocationUtil fusedLocationUtil;
    @NonNull private final PermissionDelegate permissionDelegate;

    public MainActivity() {
        googleApiClientUtil = new GoogleApiClientUtil(this, this);
        enableLocationPopup = new EnableLocationPopup(this, this);
        fusedLocationUtil = FusedLocationUtil.FusedLocationUtilBuilder.getInstance(this).withForegroundService().build();
        permissionDelegate = new PermissionDelegate(this);
        mGeofenceList = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        googleApiClientUtil.init(true);
        fusedLocationUtil.init();

        googleApiClientUtil.connect();

        populateGeofenceList();
        mGeofencingClient = LocationServices.getGeofencingClient(this);

        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LocationResult result = LocationResult.extractResult(intent);
        if (result == null) {
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (enableLocationPopup.onActivityResultDelegate(requestCode, resultCode, data)) {
            return;
        }
    }

    @Override
    public void onGoogleApiClientUtilConnectionFailed(ConnectionResult connectionResult) {
        // Log error, show Toast
    }

    @Override
    public void onGoogleApiClientUtilConnectionSuspended(int i) {
        // Log error, show Toast
    }

    @Override
    public void onGoogleApiClientUtilConnected(Bundle bundle) {
        permissionDelegate.executeActionWithPermission(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onPermissionGranted() {
        enableLocationPopup.showEnableLocationPopup(true);

        if (mGeofencingClient != null) {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnCompleteListener(this);
        }
    }

    @Override
    public void onPermissionRejected() {
        // Log error, show Toast
    }

    @Override
    public void onPermissionNeverAskAgainChecked() {
        // Log error, show Toast
    }

    @Override
    public String[] getRequestedPermissions() {
        return new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    }

    @Override
    public int getPermissionRequestCode() {
        return Definitions.REQUEST_CODE.LOCATION_PERMISSION;
    }

    @Override
    public void onEnableLocationPopupSuccess() {
        fusedLocationUtil.checkForLastKnownLocationAndRequestLocationUpdates(true);
    }

    @Override
    public void onEnableLocationPopupFailure() {
        // Log error, show Toast
    }

    @Override
    public void onComplete(@NonNull Task<Void> task) {
        if (task.isSuccessful()) {

            Toast.makeText(this, R.string.geofences_added , Toast.LENGTH_SHORT).show();
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(this, task.getException());
        }
    }

    private void populateGeofenceList() {
        for (Map.Entry<String, AccidentModel> entry : Definitions.ACCIDENT_LOCATIONS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request CHANNEL_ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getKey())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getValue().getLatitude(),
                            entry.getValue().getLongitude(),
                            Definitions.GEOFENCE.RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Definitions.GEOFENCE.EXPIRATION_IN_MILLISECONDS)

//                    .setLoiteringDelay(1000)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)

                    // Create the geofence.
                    .build());
        }
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }

        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }
}
