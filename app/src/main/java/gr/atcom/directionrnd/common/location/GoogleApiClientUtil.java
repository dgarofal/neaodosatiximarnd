package gr.atcom.directionrnd.common.location;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.lang.ref.WeakReference;

import gr.atcom.directionrnd.common.util.LocationUtils;
import gr.atcom.directionrnd.common.util.Utils;

public class GoogleApiClientUtil implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final static String TAG = "GoogleApiClientUtil";

    @Nullable
    private GoogleApiClient googleApiClient;
    @NonNull
    private IGoogleApiClientUtil listener;
    @Nullable
    private Bundle connectionBundle;
    @NonNull
    private final WeakReference<Context> contextWeakReference;

    public GoogleApiClientUtil(@NonNull Context context, @NonNull IGoogleApiClientUtil listener) {
        contextWeakReference = new WeakReference<>(context);
        this.listener = listener;
    }

    public void disconnect() {
        if (googleApiClient != null) {
//            if (listener != null) {
//                googleApiClient.stopAutoManage(listener.getGoogleApiClientHelperInterfaceFragmentActivity());
//            }
            googleApiClient.disconnect();
        }
    }

    public boolean isConnected() {
        return googleApiClient != null && googleApiClient.isConnected();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        connectionBundle = bundle;
        listener.onGoogleApiClientUtilConnected(bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {
        listener.onGoogleApiClientUtilConnectionSuspended(i);

        Log.d(TAG,"GoogleApiClientUtil -> onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        listener.onGoogleApiClientUtilConnectionFailed(connectionResult);

        Log.d(TAG, "GoogleApiClientUtil -> onConnectionFailed");
    }

    public void init(boolean showGoogleServicesUpdateDialog) {
        final Context context = Utils.weakReferenceGet(contextWeakReference);

        if (context == null) {
            return;
        }

        if (LocationUtils.googleServicesAreNotUpdated(context, showGoogleServicesUpdateDialog)) {
            return;
        }

        GoogleApiClient.Builder googleApiClientBuilder =
                new GoogleApiClient.Builder(context);

//        if (listener != null) {
//            googleApiClientBuilder.enableAutoManage(listener.getGoogleApiClientHelperInterfaceFragmentActivity(), this);
//        }

        if (googleApiClient == null) {
            googleApiClient = googleApiClientBuilder
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    public void connect() {
        if (isConnected()) {
            onConnected(connectionBundle);
        } else {
            if (googleApiClient != null) {
                googleApiClient.connect();
            }
        }
    }

    public void setConnectionListener(IGoogleApiClientUtil listener) {
        this.listener = listener;

        if (isConnected()) {
            onConnected(connectionBundle);
        } else {
            if (googleApiClient != null) {
                googleApiClient.connect();
            }
        }
    }

    @Nullable
    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public interface IGoogleApiClientUtil {
        void onGoogleApiClientUtilConnectionFailed(ConnectionResult connectionResult);

        void onGoogleApiClientUtilConnectionSuspended(int i);

        void onGoogleApiClientUtilConnected(Bundle bundle);
    }
}
