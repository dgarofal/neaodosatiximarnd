package gr.atcom.directionrnd.common.location;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.lang.ref.WeakReference;

import gr.atcom.directionrnd.common.util.Definitions;
import gr.atcom.directionrnd.common.util.LocationUtils;
import gr.atcom.directionrnd.common.util.Utils;

public final class EnableLocationPopup {

    @NonNull
    private IEnableLocationPopup listener;
    @NonNull
    private WeakReference<Activity> activityWeakReference;

    public EnableLocationPopup(@NonNull Activity activity, @NonNull IEnableLocationPopup listener) {
        this.listener = listener;
        activityWeakReference = new WeakReference<>(activity);
    }

    // Use this inside your activity
    public boolean onActivityResultDelegate(int requestCode, int resultCode, Intent data) {
        if (requestCode != Definitions.REQUEST_CODE.ENABLE_LOCATION_POPUP) {
            return false;
        }

        if (resultCode == Activity.RESULT_OK) {
            listener.onEnableLocationPopupSuccess();
            return true;
        } else {
            listener.onEnableLocationPopupFailure();
            return false;
        }
    }

    private @NonNull
    LocationSettingsRequest getLocationSettingsBuilder() {
        return new LocationSettingsRequest.Builder().addLocationRequest(LocationUtils.getLocationRequest()).build();
    }

    private @Nullable
    SettingsClient getLocationClient() {
        final Activity activity = Utils.weakReferenceGet(activityWeakReference);

        if (activity == null) {
            return null;
        }

        return LocationServices.getSettingsClient(activity);
    }

    public void showEnableLocationPopup(boolean showGoogleServicesUpdateDialog) {
        final Activity activity = Utils.weakReferenceGet(activityWeakReference);
        if (activity == null || LocationUtils.googleServicesAreNotUpdated(activity, showGoogleServicesUpdateDialog)) {
            return;
        }

        SettingsClient locationClient = getLocationClient();
        if (locationClient == null) {
            return;
        }


        final Task<LocationSettingsResponse> task = locationClient.checkLocationSettings(getLocationSettingsBuilder());
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                listener.onEnableLocationPopupSuccess();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ((ResolvableApiException) e)
                                .startResolutionForResult(
                                        activity,
                                        Definitions.REQUEST_CODE.ENABLE_LOCATION_POPUP);
                    } catch (IntentSender.SendIntentException sendException) {
                        Log.d("EnableLocationPopup", sendException.getMessage());
                        listener.onEnableLocationPopupFailure();
                    }
                }
            }
        });
    }

    public interface IEnableLocationPopup {
        void onEnableLocationPopupSuccess();

        void onEnableLocationPopupFailure();
    }
}
