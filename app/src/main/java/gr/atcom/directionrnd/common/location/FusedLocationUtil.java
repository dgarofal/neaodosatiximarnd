package gr.atcom.directionrnd.common.location;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.lang.ref.WeakReference;

import gr.atcom.directionrnd.common.util.Definitions;
import gr.atcom.directionrnd.common.util.LocationUtils;
import gr.atcom.directionrnd.common.util.Utils;

public class FusedLocationUtil {

    public static class FusedLocationUtilBuilder {
        private @NonNull
        FusedLocationUtil fusedLocationUtil;

        private FusedLocationUtilBuilder(@NonNull Context context) {
            fusedLocationUtil = new FusedLocationUtil(context);
        }

        public static FusedLocationUtilBuilder getInstance(@NonNull Context context) {
            return new FusedLocationUtilBuilder(context);
        }

        @NonNull
        public FusedLocationUtilBuilder withBroadcastReceiver() {
            fusedLocationUtil.withBroadcastReceiver = true;
            fusedLocationUtil.withForegroundService = false;
            return this;
        }

        @NonNull
        public FusedLocationUtilBuilder withForegroundService() {
            fusedLocationUtil.withBroadcastReceiver = false;
            fusedLocationUtil.withForegroundService = true;
            return this;
        }

        @NonNull
        public FusedLocationUtilBuilder withListener(@Nullable IFusedLocationUtil listener) {
            fusedLocationUtil.listener = listener;
            fusedLocationUtil.withBroadcastReceiver = false;
            fusedLocationUtil.withForegroundService = false;
            return this;
        }

        @NonNull
        public FusedLocationUtil build() {
             return fusedLocationUtil;
        }
    }


    @Nullable
    private IFusedLocationUtil listener;
    @NonNull
    private WeakReference<Context> contextWeakReference;
    @Nullable
    private Looper looper;
    @Nullable
    private LocationCallback locationCallback;
    @Nullable
    private FusedLocationProviderClient fusedLocationProviderClient;
    private boolean withBroadcastReceiver;
    private boolean withForegroundService;

    public FusedLocationUtil(@NonNull Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    public void init() {
        final Context context = Utils.weakReferenceGet(contextWeakReference);
        if (context == null) {
            return;
        }

        fusedLocationProviderClient = new FusedLocationProviderClient(context);

        this.locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                if (FusedLocationUtil.this.listener != null) {
                    FusedLocationUtil.this.listener.onFusedLocationUtilSuccess(locationResult.getLastLocation());
                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);

                if (!locationAvailability.isLocationAvailable()) {
                    fusedLocationProviderClient.removeLocationUpdates(this);
                }
            }
        };
    }

    @SuppressLint("MissingPermission")
    public void checkForLastKnownLocationAndRequestLocationUpdates(boolean showGoogleServicesUpdateDialog) {

        final Context context = Utils.weakReferenceGet(contextWeakReference);
        if (context == null) {
            return;
        }

        if (LocationUtils.googleServicesAreNotUpdated(context, showGoogleServicesUpdateDialog)) {
            return;
        }

        if (LocationUtils.locationPermissionIsNotGranted(context)) {
            if (listener != null) {
                listener.onFusedLocationUtilFailure();
            }
            return;
        }

        if (fusedLocationProviderClient == null) {
            return;
        }

        final Task<Location> askForLocationTask = fusedLocationProviderClient.getLastLocation();

        askForLocationTask.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location == null ||
                        SystemClock.elapsedRealtimeNanos() - location.getElapsedRealtimeNanos() > Definitions.LOCATION_REQUEST.MAX_AGE) {

                    requestLocationUpdates(null);
                    return;
                }

                if (listener != null) {
                    listener.onFusedLocationUtilSuccess(location);
                }

                requestLocationUpdates(location);
            }
        });

        askForLocationTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                requestLocationUpdates(null);
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates(@Nullable Location location) {
        final Context context = Utils.weakReferenceGet(contextWeakReference);
        if (LocationUtils.locationPermissionIsNotGranted(context)) {
            if (listener != null) {
                listener.onFusedLocationUtilFailure();
            }
            return;
        }

        if (fusedLocationProviderClient == null) {
            return;
        }

        LocationRequest locationRequest = LocationUtils.getLocationRequest();
        if (listener != null && locationCallback != null) {
            fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    looper);

            return;
        }

        if (withBroadcastReceiver) {
            Intent intent = new Intent(context, LocationBroadcastReceiver.class);
            intent.setAction(LocationBroadcastReceiver.ACTION_PROCESS_UPDATES);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest, pendingIntent
            );
        } else if (withForegroundService) {
            Intent intent = new Intent(context, LocationForegroundService.class);
            intent.setAction(LocationBroadcastReceiver.ACTION_PROCESS_UPDATES);

            PendingIntent pendingIntent;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                pendingIntent = PendingIntent.getForegroundService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest, pendingIntent
            );

        }
    }

    public void removeLocationUpdates() {
        if (locationCallback != null && fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    public interface IFusedLocationUtil {
        void onFusedLocationUtilSuccess(@Nullable Location location);

        void onFusedLocationUtilFailure();
    }

}
