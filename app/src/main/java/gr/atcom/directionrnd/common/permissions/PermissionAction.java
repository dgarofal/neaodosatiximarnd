package gr.atcom.directionrnd.common.permissions;

public interface PermissionAction {
    void onPermissionGranted();

    void onPermissionRejected();

    void onPermissionNeverAskAgainChecked();

    String[] getRequestedPermissions();

    int getPermissionRequestCode();

}
