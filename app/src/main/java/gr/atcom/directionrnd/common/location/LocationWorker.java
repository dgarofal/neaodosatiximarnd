package gr.atcom.directionrnd.common.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;

import androidx.work.Worker;

public class LocationWorker extends Worker implements GoogleApiClientUtil.IGoogleApiClientUtil, FusedLocationUtil.IFusedLocationUtil {

    @Nullable private GoogleApiClientUtil googleApiClientUtil;
    @Nullable private FusedLocationUtil fusedLocationUtil;

    @NonNull
    @Override
    public Result doWork() {
        final Context context = getApplicationContext();

        if (fusedLocationUtil == null) {
            fusedLocationUtil = FusedLocationUtil.FusedLocationUtilBuilder.getInstance(context).withListener(this).build();
            fusedLocationUtil.init();
        }

        if (googleApiClientUtil == null) {
            googleApiClientUtil = new GoogleApiClientUtil(context, this);
            googleApiClientUtil.init(false);
        }

        googleApiClientUtil.connect();

        return Result.SUCCESS;
    }

    @Override
    public void onGoogleApiClientUtilConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onGoogleApiClientUtilConnectionSuspended(int i) {
    }

    @Override
    public void onGoogleApiClientUtilConnected(Bundle bundle) {
        if (fusedLocationUtil != null) {
            fusedLocationUtil.checkForLastKnownLocationAndRequestLocationUpdates(false);
        }
    }

    @Override
    public void onFusedLocationUtilSuccess(@Nullable Location location) {
        if (location == null) {
            return;
        }

        Log.e("LocationWorker", location.toString());
    }

    @Override
    public void onFusedLocationUtilFailure() {
        Log.e("LocationWorker", "onLocationWorkerFailure");
    }
}
