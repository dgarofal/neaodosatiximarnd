package gr.atcom.directionrnd.common.location;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.LocationResult;

import gr.atcom.directionrnd.MainActivity;
import gr.atcom.directionrnd.common.db.DatabaseHelper;
import gr.atcom.directionrnd.common.util.Definitions;
import gr.atcom.directionrnd.common.util.NotificationUtils;

public class LocationForegroundService extends Service {

    private @Nullable DatabaseHelper databaseHelper;
    private final static String LOG_TAG = "LocationService";

    @Override
    public void onCreate() {
        super.onCreate();

        databaseHelper = DatabaseHelper.getInstance(getApplicationContext());
        databaseHelper.onInit(databaseHelper.getWritableDatabase());

        startForeground(Definitions.NOTIFICATION.ID, getNotification());

    }

    public Notification getNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationUtils.createNotificationChannel(getApplicationContext());
        }

        PendingIntent pendingIntent =
                NotificationUtils.getPendingIntentForNotification(this, MainActivity.class);

        NotificationCompat.Builder notificationBuilder = NotificationUtils.getNotificationBuilder(getApplicationContext())
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_LOW);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_SERVICE);
        }

        return notificationBuilder.build();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return Service.START_STICKY;
        }

        LocationResult result = LocationResult.extractResult(intent);
        if (result == null || databaseHelper == null) {
            return Service.START_STICKY;
        }

        databaseHelper.insert(new LocationWith2Neighbors(result.getLastLocation()));

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (databaseHelper != null) {
            try {
                databaseHelper.close();
            } catch (Exception e) {
                //
            }
        }

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
