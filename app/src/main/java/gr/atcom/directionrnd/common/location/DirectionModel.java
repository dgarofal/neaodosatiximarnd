package gr.atcom.directionrnd.common.location;

public class DirectionModel {
    private int anodes;
    private int cathodes;

    public DirectionModel() {}

    public int getAnodes() {
        return anodes;
    }

    public void setAnodes(int anodes) {
        this.anodes = anodes;
    }

    public int getCathodes() {
        return cathodes;
    }

    public void setCathodes(int cathodes) {
        this.cathodes = cathodes;
    }

    public void addAnode() {
        this.anodes++;
    }

    public void addCathode() {
        this.cathodes++;
    }
}
