package gr.atcom.directionrnd.common.util;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import gr.atcom.directionrnd.common.location.LocationWorker;

public class Utils {
    public @Nullable
    static AppCompatActivity getAppCompatActivityFrom(@Nullable Context context) {
        if (context instanceof ContextThemeWrapper) {
            return getAppCompatActivityFrom(((ContextThemeWrapper) context).getBaseContext());
        } else if (context instanceof android.view.ContextThemeWrapper && !(context instanceof AppCompatActivity)) {
            return getAppCompatActivityFrom(((android.view.ContextThemeWrapper) context).getBaseContext());
        } else if (!(context instanceof AppCompatActivity)) {
            return null;
        }

        AppCompatActivity appCompatActivity = (AppCompatActivity) context;

        if (appCompatActivity.isFinishing() || appCompatActivity.isDestroyed()) {
            return null;
        }

        return appCompatActivity;
    }

    public static @Nullable
    <T> T weakReferenceGet(@Nullable WeakReference<T> weakReference) {
        if (weakReference == null || weakReference.isEnqueued()) {
            return null;
        }

        return weakReference.get();
    }


    public static void startWorkerManagerForLocation() {
        PeriodicWorkRequest periodicWorkRequest =
                new PeriodicWorkRequest.Builder(LocationWorker.class, 2000, TimeUnit.MILLISECONDS).build();

        WorkManager.getInstance().enqueueUniquePeriodicWork(
                Definitions.WORK_NAMAGER.NAME,
                ExistingPeriodicWorkPolicy.REPLACE,
                periodicWorkRequest);
    }

    public static void stopWorkerManagerForLocation() {
        WorkManager.getInstance().cancelUniqueWork(Definitions.WORK_NAMAGER.NAME);
    }



}
