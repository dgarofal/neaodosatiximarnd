package gr.atcom.directionrnd.common.location;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Objects;

public class LocationWeightModel extends Location implements Comparable<LocationWeightModel> {
    private int weight;
    private float distance;

    public LocationWeightModel() {
        super("");
    }

    public LocationWeightModel(int weight) {
        super("");
        this.weight = weight;
    }

    public LocationWeightModel(LocationWeightModel location) {
        super(location);
        this.weight = location.getWeight();
        this.distance = location.getDistance();
    }

    public LocationWeightModel(LocationWeightModel m, int weight, float distance) {
        super(m);
        this.weight = weight;
        this.distance = distance;
    }

    public LocationWeightModel(LocationWeightModel m, float distance) {
        super(m);
        this.weight = m.getWeight();
        this.distance = distance;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationWeightModel that = (LocationWeightModel) o;
        return weight == that.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, distance);
    }


    @Override
    public int compareTo(@NonNull LocationWeightModel o) {
        if (o.weight > weight)
            return -1;
        else if (o.weight < weight)
            return 1;

        return equals(o) ? 0 : 1;
    }
}
