package gr.atcom.directionrnd.common.location;

import android.location.Location;
import android.support.annotation.NonNull;

public enum DirectionEnum {
    ANODE, CATHODE, UNDETERMINED;

    public static @NonNull
    DirectionEnum getDirectionEnumFrom(@NonNull Location location1, @NonNull Location location2) {
        float bearing = location1.bearingTo(location2);

        if (bearing < 0) {
            bearing += 360;
        }

        if (bearing > 270 || bearing < 90) {
            return ANODE;
        } else  {
            return CATHODE;
        }
    }

    public static @NonNull
    DirectionEnum getDirectionEnumFrom(@NonNull DirectionModel directionModel) {
        if (directionModel.getAnodes() > directionModel.getCathodes()) {
            return DirectionEnum.ANODE;
        } else if (directionModel.getAnodes() > directionModel.getCathodes()) {
            return DirectionEnum.CATHODE;
        } else {
            return DirectionEnum.UNDETERMINED;
        }
    }
}
