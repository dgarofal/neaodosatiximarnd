package gr.atcom.directionrnd.common.geofence;

import android.location.Location;
import android.support.annotation.NonNull;

import gr.atcom.directionrnd.common.location.DirectionEnum;

public class AccidentModel extends Location {
    private @NonNull final DirectionEnum directionEnum;

    public AccidentModel(@NonNull DirectionEnum directionEnum) {
        super("");
        this.directionEnum = directionEnum;
    }

    public AccidentModel(@NonNull Location location, @NonNull DirectionEnum directionEnum) {
        super(location);
        this.directionEnum = directionEnum;
    }
}
