package gr.atcom.directionrnd.common.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationRequest;

public class LocationUtils {
    public static @NonNull
    LocationRequest getLocationRequest() {
        return LocationRequest.create()
                .setInterval(Definitions.LOCATION_REQUEST.INTERVAL)
                .setFastestInterval(Definitions.LOCATION_REQUEST.FASTEST_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setMaxWaitTime(Definitions.LOCATION_REQUEST.MAX_WAIT_TIME)
                .setSmallestDisplacement(Definitions.LOCATION_REQUEST.SMALLEST_DISPLACEMENT);
    }

    public static boolean locationPermissionIsNotGranted(@Nullable Context context) {
        return context == null ||
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    public static boolean googleServicesAreNotUpdated(@Nullable Context context, boolean updateDialog) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();

        int status = googleAPI.isGooglePlayServicesAvailable(context);

        final boolean updated = status != ConnectionResult.SUCCESS;
        if (updateDialog) {
            showGoogleServicesUpdateDialog(context, status);
        }

        return updated;
    }

    public static void showGoogleServicesUpdateDialog(@Nullable Context context, int status) {

        final AppCompatActivity activity = Utils.getAppCompatActivityFrom(context);

        if (activity == null) {
            return;
        }

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();


        if (googleAPI.isUserResolvableError(status)) {
            googleAPI.getErrorDialog(activity, status, Definitions.REQUEST_CODE.UPDATE_GOOGLE_SERVICES).show();
        }
    }

}
