package gr.atcom.directionrnd.common.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import gr.atcom.directionrnd.MainActivity;
import gr.atcom.directionrnd.R;

public class NotificationUtils {
    @RequiresApi(26)
    public static synchronized void createNotificationChannel(@Nullable Context context) {
        if (context == null) {
            return;
        }

        NotificationManager mNotificationManager = getNotificationManager(context);
        if (mNotificationManager == null) {
            return;
        }

        NotificationChannel mChannel =
                new NotificationChannel(
                        Definitions.NOTIFICATION.CHANNEL_ID,
                        context.getString(R.string.location_foreground_service_visible_name),
                        NotificationManager.IMPORTANCE_LOW);

//        mChannel.enableLights(true);
//        mChannel.setLightColor(Color.BLUE);

        mNotificationManager.createNotificationChannel(mChannel);
    }

    public static @NonNull
    NotificationCompat.Builder getNotificationBuilder(@NonNull Context context) {
        return new NotificationCompat.Builder(context, Definitions.NOTIFICATION.CHANNEL_ID);
    }

    public static @Nullable
    PendingIntent getPendingIntentForNotification(@Nullable Context context, Class<?> activityClass) {
        if (context == null) {
            return null;
        }

        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(context, activityClass);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    public static @Nullable NotificationManager getNotificationManager(@Nullable Context context) {

        if (context == null) {
            return null;
        }


        Object systemService = context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (systemService == null) {
            return null;
        }

        return (NotificationManager) systemService;
    }

}
