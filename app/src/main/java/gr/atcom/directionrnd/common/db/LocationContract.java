package gr.atcom.directionrnd.common.db;

import android.provider.BaseColumns;

public class LocationContract {

    private LocationContract() {}

    public static class LocationEntry implements BaseColumns {
        public static final String TABLE_NAME = "location";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
    }
}
