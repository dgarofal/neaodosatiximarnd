package gr.atcom.directionrnd.common.util;

import android.location.Location;
import android.location.LocationManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gr.atcom.directionrnd.common.location.DirectionEnum;
import gr.atcom.directionrnd.common.geofence.AccidentModel;
import gr.atcom.directionrnd.common.location.LocationWeightModel;

public class Definitions {
    public interface REQUEST_CODE {
        int UPDATE_GOOGLE_SERVICES = 775;
        int ENABLE_LOCATION_POPUP = 776;
        int LOCATION_PERMISSION = 777;
    }

    public interface LOCATION_REQUEST {
        int INTERVAL = 2000; // ms
        int FASTEST_INTERVAL = 500; // ms
        int MAX_WAIT_TIME = INTERVAL * 3; // ms
        long MAX_AGE = 5 * 60 * 1000; // 5 minutes
        float SMALLEST_DISPLACEMENT = 1000; // meters

    }

    public interface WORK_NAMAGER {
        // Work manager
        String NAME = "gr.atcom.directionrnd.common.location.LocationWorker";

    }

    public interface NOTIFICATION {
        int ID = 4;
        String CHANNEL_ID = "gr.atcom.directionrnd.common.location.NotificationChannelId";
    }

    public interface GEOFENCE {
        /**
         * For this sample, geofences expire after twelve hours.
         */
        long EXPIRATION_IN_HOURS = 12;
        long EXPIRATION_IN_MILLISECONDS = EXPIRATION_IN_HOURS * 60 * 60 * 1000;
        float RADIUS_IN_METERS = 1609; // 1 mile, 1.6 km
    }

    public static HashMap<String, AccidentModel> ACCIDENT_LOCATIONS = new HashMap<>();
    public static List<LocationWeightModel> ioniaSelectedPoints = new ArrayList<>();

    static {
        Location accidentLocation = new Location(LocationManager.GPS_PROVIDER);
        accidentLocation.setLatitude(38.495723);
        accidentLocation.setLongitude(21.338625);

        ACCIDENT_LOCATIONS.put("ACCIDENT 1", new AccidentModel(accidentLocation, DirectionEnum.ANODE));


        LocationWeightModel location = new LocationWeightModel(1);
        location.setLatitude(38.330524);
        location.setLongitude(21.767754);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(2);
        location.setLatitude(38.387824);
        location.setLongitude(21.602225);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(3);
        location.setLatitude(38.393394);
        location.setLongitude(21.429855);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(4);
        location.setLatitude(38.505675);
        location.setLongitude(21.314259);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(5);
        location.setLatitude(38.63255);
        location.setLongitude(21.235689);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(6);
        location.setLatitude(38.980001);
        location.setLongitude(21.174648);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(7);
        location.setLatitude(39.095559);
        location.setLongitude(21.067428);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(8);
        location.setLatitude(39.185082);
        location.setLongitude(20.929061);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(9);
        location.setLatitude(39.309122);
        location.setLongitude(20.905786);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(10);
        location.setLatitude(39.448045);
        location.setLongitude(20.903919);
        ioniaSelectedPoints.add(location);

        location = new LocationWeightModel(11);
        location.setLatitude(39.589827);
        location.setLongitude(20.822951);
        ioniaSelectedPoints.add(location);
    }
}
