package gr.atcom.directionrnd.common.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import gr.atcom.directionrnd.MainActivity;

public class LocationBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "LocationBR";
    public static final String ACTION_PROCESS_UPDATES = "gr.atcom.directionrnd.action.ACTION_PROCESS_UPDATES";

    @Override
    public void onReceive(Context context, Intent intent) {

        final Intent mainActivityIntent = new Intent(context, MainActivity.class);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mainActivityIntent.putExtras(intent);
        try {
            context.startActivity(mainActivityIntent);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }

    }
}
