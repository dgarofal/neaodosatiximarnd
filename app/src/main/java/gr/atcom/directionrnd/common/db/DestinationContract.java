package gr.atcom.directionrnd.common.db;

import android.provider.BaseColumns;

public class DestinationContract {
    private DestinationContract() {}

    public static class DestinationEntry implements BaseColumns {
        public static final String TABLE_NAME = "destination";
        public static final String CLOSEST_NEIGHBOR_WEIGHT = "closest_neighbor_weight";
        public static final String CLOSEST_NEIGHBOR_DISTANCE = "closest_neighbor_distance";
        public static final String SECOND_CLOSEST_NEIGHBOR_WEIGHT = "second_closest_neighbor_weight";
        public static final String SECOND_CLOSEST_NEIGHBOR_DISTANCE = "second_closest_neighbor_distance";
        public static final String LOCATION_FOREIGN_KEY = "location";
    }
}
