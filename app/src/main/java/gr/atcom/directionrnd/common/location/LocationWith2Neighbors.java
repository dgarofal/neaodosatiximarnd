package gr.atcom.directionrnd.common.location;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.Iterator;

import gr.atcom.directionrnd.common.db.DestinationContract;
import gr.atcom.directionrnd.common.db.LocationContract;
import gr.atcom.directionrnd.common.util.Definitions;

public class LocationWith2Neighbors extends Location {
    private final @NonNull LocationWeightModel[] neighbors;

    public LocationWith2Neighbors() {
        super("");
        neighbors = new LocationWeightModel[2];
    }

    public LocationWith2Neighbors(Location l) {
        super(l);
        neighbors = new LocationWeightModel[2];
    }

    public void addNeighbor(@Nullable LocationWeightModel locationWeightModel) {
        if (locationWeightModel == null) {
            return;
        }

        if (neighbors[0] == null) {
            neighbors[0] = locationWeightModel;
        } else if (neighbors[1] == null) {
            neighbors[1] = locationWeightModel;
            Arrays.sort(neighbors);
        }
    }

    @NonNull
    public LocationWeightModel[] getNeighbors() {
        return neighbors;
    }

    public void insert(@NonNull SQLiteDatabase db ) {
        ContentValues values = new ContentValues();
        values.put(LocationContract.LocationEntry.LATITUDE, getLatitude());
        values.put(LocationContract.LocationEntry.LONGITUDE, getLongitude());

        long locationId = db.insert(LocationContract.LocationEntry.TABLE_NAME, null, values);

        setClosestNeighbors();

        if (getNeighbors()[0] != null && getNeighbors()[1] != null) {
            ContentValues destinationValues = new ContentValues();

            destinationValues.put(DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_WEIGHT, getNeighbors()[0].getWeight());
            destinationValues.put(DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_DISTANCE, getNeighbors()[0].getDistance());

            destinationValues.put(DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_WEIGHT, getNeighbors()[1].getWeight());
            destinationValues.put(DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_DISTANCE, getNeighbors()[1].getDistance());

            destinationValues.put(DestinationContract.DestinationEntry.LOCATION_FOREIGN_KEY, locationId);

            db.insert(DestinationContract.DestinationEntry.TABLE_NAME, null, destinationValues);
        }
    }

    private void setClosestNeighbors() {
        LocationWeightModel closestNeighbor = getClosestNeighbor(null);
        addNeighbor(closestNeighbor);

        LocationWeightModel secondClosestNeighbor = getClosestNeighbor(closestNeighbor);
        addNeighbor(secondClosestNeighbor);
    }

    private @Nullable LocationWeightModel getClosestNeighbor(@Nullable LocationWeightModel alreadyFoundClosestNeighbor) {
        Iterator<LocationWeightModel> iterator = Definitions.ioniaSelectedPoints.iterator();
        if (!iterator.hasNext()) {
            return null;
        }

        LocationWeightModel closestNeighbor = new LocationWeightModel(iterator.next());
        closestNeighbor.setDistance(distanceTo(closestNeighbor));

        if (closestNeighbor.equals(alreadyFoundClosestNeighbor)) {
            closestNeighbor = null;
        }

        while (iterator.hasNext()) {
            LocationWeightModel m = iterator.next();

            float distance = distanceTo(m);

            if (closestNeighbor == null ||
                    closestNeighbor.getDistance() > distance && !m.equals(alreadyFoundClosestNeighbor)) {

                closestNeighbor = new LocationWeightModel(m, distance);
            }
        }

        return closestNeighbor;
    }

    // Anode means going from the bigger weight to the smaller one
    public static boolean isAnode(@NonNull LocationWith2Neighbors one, @NonNull LocationWith2Neighbors two) {
        if (neighborsAreNull(one, two)) {
            return false;
        }

        return
                one.getNeighbors()[0].getDistance() > two.getNeighbors()[0].getDistance()
                        &&
                        one.getNeighbors()[1].getDistance() < two.getNeighbors()[1].getDistance();
    }

    // Cathode means going from the smaller weight to the bigger one
    public static boolean isCathode(@NonNull LocationWith2Neighbors one, @NonNull LocationWith2Neighbors two) {
        if (neighborsAreNull(one, two)) {
            return false;
        }

        return
                one.getNeighbors()[0].getDistance() < two.getNeighbors()[0].getDistance() &&
                        one.getNeighbors()[1].getDistance() > two.getNeighbors()[1].getDistance();
    }

    private static boolean neighborsAreNull(@NonNull LocationWith2Neighbors one, @NonNull LocationWith2Neighbors two) {
        return
                one.getNeighbors()[0] == null || two.getNeighbors()[0] == null ||
                        one.getNeighbors()[1] == null || two.getNeighbors()[1] == null;
    }

    private static boolean neighborsHaveEqualWeights(@NonNull LocationWith2Neighbors one, @NonNull LocationWith2Neighbors two) {
        return
                one.getNeighbors()[0].getWeight() == two.getNeighbors()[0].getWeight() &&
                        one.getNeighbors()[1].getWeight() == two.getNeighbors()[1].getWeight();
    }

    public static DirectionModel computeDirection(@NonNull Iterator<LocationWith2Neighbors> iterator,
                                                  @Nullable LocationWith2Neighbors one,
                                                  @Nullable LocationWith2Neighbors two) {
        DirectionModel directionModel = new DirectionModel();
        if (one == null || two == null) {
            return directionModel;
        }

        while (iterator.hasNext()) {
            if (neighborsAreNull(one, two)) {
                continue;
            }

            if (neighborsHaveEqualWeights(one, two)) {

                if (LocationWith2Neighbors.isAnode(one, two)) {
                    directionModel.addAnode();
                } else if (LocationWith2Neighbors.isCathode(one, two))
                    directionModel.addCathode();
            }

            one = two;
            two = iterator.next();
        }

        return directionModel;
    }


}
