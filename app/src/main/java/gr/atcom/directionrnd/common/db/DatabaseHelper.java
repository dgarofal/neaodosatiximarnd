package gr.atcom.directionrnd.common.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

import gr.atcom.directionrnd.common.location.DirectionModel;
import gr.atcom.directionrnd.common.location.DirectionEnum;
import gr.atcom.directionrnd.common.location.LocationWeightModel;
import gr.atcom.directionrnd.common.location.LocationWith2Neighbors;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "NeaOdosAccidents.db";

    public static final String SQL_CREATE_LOCATION_ENTRIES =
            "CREATE TABLE " + LocationContract.LocationEntry.TABLE_NAME + " (" +
                    LocationContract.LocationEntry._ID + " INTEGER PRIMARY KEY," +
                    LocationContract.LocationEntry.LATITUDE + " DOUBLE," +
                    LocationContract.LocationEntry.LONGITUDE + " DOUBLE);";

    public static final String SQL_DELETE_LOCATION_ENTRIES =
            "DROP TABLE IF EXISTS " + LocationContract.LocationEntry.TABLE_NAME;

    public static final String SQL_CREATE_DIRECTION_ENTRIES =
            "CREATE TABLE " + DestinationContract.DestinationEntry.TABLE_NAME + " (" +
                    DestinationContract.DestinationEntry._ID + " INTEGER PRIMARY KEY," +
                    DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_DISTANCE + " FLOAT," +
                    DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_WEIGHT + " INTEGER," +
                    DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_DISTANCE + " FLOAT," +
                    DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_WEIGHT + " INTEGER," +
                    DestinationContract.DestinationEntry.LOCATION_FOREIGN_KEY + " REFERENCES " +
                    LocationContract.LocationEntry.TABLE_NAME + "(" + LocationContract.LocationEntry._ID + "));";

    public static final String SQL_DELETE_DIRECTION_ENTRIES =
            "DROP TABLE IF EXISTS " + DestinationContract.DestinationEntry.TABLE_NAME;


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_LOCATION_ENTRIES);
        db.execSQL(SQL_CREATE_DIRECTION_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for temporary data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_LOCATION_ENTRIES);
        db.execSQL(SQL_DELETE_DIRECTION_ENTRIES);
        onCreate(db);
    }

    public void onInit(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_LOCATION_ENTRIES);
        db.execSQL(SQL_DELETE_DIRECTION_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public static synchronized @NonNull
    DatabaseHelper getInstance(@NonNull Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }

        return sInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    public void insert(@NonNull LocationWith2Neighbors location) {
        location.insert(getWritableDatabase());
        determineDirection();
    }


    public @NonNull
    DirectionEnum determineDirection() {

        ArrayList<LocationWith2Neighbors> locations = getAllLocations();

        final Iterator<LocationWith2Neighbors> iterator = locations.iterator();

        LocationWith2Neighbors one = null;
        LocationWith2Neighbors two = null;

        if (iterator.hasNext()) {
            one = iterator.next();
        }

        if (iterator.hasNext()) {
            two = iterator.next();
        }

        DirectionModel directionModel = LocationWith2Neighbors.computeDirection(iterator, one, two);

        Log.d("Mits anodes", String.valueOf(directionModel.getAnodes()));
        Log.d("Mits cathodes", String.valueOf(directionModel.getCathodes()));

        return DirectionEnum.getDirectionEnumFrom(directionModel);
    }

    private @NonNull ArrayList<LocationWith2Neighbors> getAllLocations() {
        SQLiteDatabase db = getReadableDatabase();
        String[] projection = getDestinationProjection();
        String sortOrder =
                DestinationContract.DestinationEntry.LOCATION_FOREIGN_KEY + " ASC";

        Cursor cursor = db.query(DestinationContract.DestinationEntry.TABLE_NAME, projection,
                null, null, null, null, sortOrder);

        cursor.moveToFirst();

        ArrayList<LocationWith2Neighbors> locations = new ArrayList<>();
        do {

            locations.add(getLocationWithNeighborsFrom(cursor));

        } while (cursor.moveToNext());
        cursor.close();

        return locations;
    }

    private static @NonNull
    LocationWith2Neighbors getLocationWithNeighborsFrom(@NonNull Cursor cursor) {
        LocationWith2Neighbors location = new LocationWith2Neighbors();
        Double latitude = getLatitudeFrom(cursor);
        if (latitude != null) {
            location.setLatitude(latitude);
        }
        Double longitude = getLongitudeFrom(cursor);
        if (longitude != null) {
            location.setLongitude(longitude);
        }

        LocationWeightModel locationWeightModel = new LocationWeightModel();
        Integer weight = getClosestWeight(cursor);
        if (weight != null) {
            locationWeightModel.setWeight(weight);
        }
        Float distance = getClosestDistanceFrom(cursor);
        if (distance != null) {
            locationWeightModel.setDistance(distance);
        }

        location.addNeighbor(locationWeightModel);

        locationWeightModel = new LocationWeightModel();
        Integer nextWeight = getSecondClosestWeight(cursor);
        if (nextWeight != null) {
            locationWeightModel.setWeight(nextWeight);
        }
        Float nextDistance = getSecondClosestDistanceFrom(cursor);
        if (nextDistance != null) {
            locationWeightModel.setDistance(nextDistance);
        }

        location.addNeighbor(locationWeightModel);

        return location;
    }


    private static @Nullable
    Double getLatitudeFrom(@NonNull Cursor cursor) {
        try {
            return cursor.getDouble(cursor.getColumnIndex(LocationContract.LocationEntry.LATITUDE));
        } catch (Exception e) {
            return null;
        }
    }

    private static @Nullable
    Double getLongitudeFrom(@NonNull Cursor cursor) {
        try {
            return cursor.getDouble(cursor.getColumnIndex(LocationContract.LocationEntry.LONGITUDE));
        } catch (Exception e) {
            return null;
        }
    }

    private static @Nullable
    Integer getClosestWeight(@NonNull Cursor cursor) {
        try {
            return cursor.getInt(cursor.getColumnIndex(DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_WEIGHT));
        } catch (Exception e) {
            return null;
        }
    }

    private static @Nullable
    Float getClosestDistanceFrom(@NonNull Cursor cursor) {
        try {
            return cursor.getFloat(cursor.getColumnIndex(DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_DISTANCE));
        } catch (Exception e) {
            return null;
        }
    }

    private static @Nullable
    Integer getSecondClosestWeight(@NonNull Cursor cursor) {
        try {
            return cursor.getInt(cursor.getColumnIndex(DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_WEIGHT));
        } catch (Exception e) {
            return null;
        }
    }

    private static @Nullable
    Float getSecondClosestDistanceFrom(@NonNull Cursor cursor) {
        try {
            return cursor.getFloat(cursor.getColumnIndex(DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_DISTANCE));
        } catch (Exception e) {
            return null;
        }
    }

    private @NonNull
    String[] getDestinationProjection() {
        return new String[]{
                DestinationContract.DestinationEntry._ID,
                DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_WEIGHT,
                DestinationContract.DestinationEntry.CLOSEST_NEIGHBOR_DISTANCE,
                DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_WEIGHT,
                DestinationContract.DestinationEntry.SECOND_CLOSEST_NEIGHBOR_DISTANCE
        };
    }
}