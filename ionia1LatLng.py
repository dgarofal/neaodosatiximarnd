#!/bin/sh

lat = ""
lng = ""

reading_lat = False
reading_lng = False

locations = []


class Location:
	def __init__(self, lat, lng):
		self.lat = lat
		self.lng = lng
	def __str__(self):
		return self.lat + ", " + self.lng
	def __uni__(self):
		return self.lat + ", " + self.lng

with open("ionia1LatLng") as f:
	while True:
		c = f.read(1)
		if not c:
			break
		else:

			if c == "[":
				reading_lat = True
				c = f.read(1)
			elif c == "," and reading_lat:
				reading_lat = False
				reading_lng = True
				c = f.read(1)
			elif c == "]" and reading_lng:
				reading_lat = False
				reading_lng = False
				locations.append(Location(lat.strip(), lng.strip()))
				lat = ""
				lng = ""
				c = f.read(1)

			if reading_lat:
				lat += c
			elif reading_lng:
				lng += c


def createPOJO(locations):
	f = open("ionia_selected_points", "w")

	f.write("""Location location = new Location(LocationManager.GPS_PROVIDER);""")
	for i in (0, 170, 340, 510, 680, 850, 1020, 1190, 1360, 1530, 1716):
		f.write("""
location.setLatitude(%s);
location.setLongitude(%s);\n""" % (locations[i].lat, locations[i].lng))

def createGpx(locations):
	header="""<?xml version="1.0"?>
	<gpx
	  version="1.0"
	  creator="mitsest:  mitsest@github.com"
	  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	  xmlns="http://www.topografix.com/GPX/1/0"
	  xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd">
	"""

	bounds="""
	<bounds minlat="%s" minlon="%s" maxlat="%s" maxlon="%s"/>
	""" % (locations[0].lat, locations[0].lng, locations[len(locations) - 1].lat, locations[len(locations) - 1].lng)

	start="""
	<wpt lat="%s" lon="%s">
	   <name>START</name>
	</wpt>
	""" % (locations[0].lat, locations[0].lng)

	body="""
	<rte>
	  <name>Ionia</name>
	"""

	for i, l in enumerate(reversed(locations)):
		body += """
	    <rtept lat="%s" lon="%s">
	      <name>%d</name>
	      <cmt>%d</cmt>
	    </rtept>
	""" % (l.lat, l.lng, i, i)
		
	body += "</rte></gpx>"

	f = open('ionia.gpx', 'w')
	f.write(header)
	f.write(bounds)
	f.write(start)
	f.write(body)
	f.close()

createPOJO(locations)
